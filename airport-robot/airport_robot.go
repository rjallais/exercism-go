package airportrobot

import "fmt"

type Italian struct {
}

type Portuguese struct {
}

type Greeter interface {
	LanguageName() string
	Greet(name string) string
}

func SayHello(name string, greeter Greeter) string {
	return fmt.Sprintf("I can speak %s: %s", greeter.LanguageName(), greeter.Greet(name))
}

func (i Italian) Greet(name string) string {
	return "Ciao " + name + "!"
}

func (i Italian) LanguageName() string {
	return "Italian"
}

func (p Portuguese) Greet(name string) string {
	return "Olá " + name + "!"
}

func (p Portuguese) LanguageName() string {
	return "Portuguese"
}
